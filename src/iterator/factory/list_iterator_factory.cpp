#include "list_iterator_factory.h"
#include "../../shape.h"
#include "../null_iterator.h"
#include "../list_compound_iterator.h"

#include <list>

ListIteratorFactory ListIteratorFactory::ListInstance;

ListIteratorFactory::ListIteratorFactory(){ _register("List", this); }

Iterator* ListIteratorFactory::createIterator(){
    return new NullIterator();
}

Iterator* ListIteratorFactory::createIterator(std::list<Shape *>::const_iterator begin, std::list<Shape *>::const_iterator end){
    return new ListCompoundIterator<std::list<Shape *>::const_iterator>(begin, end);
}