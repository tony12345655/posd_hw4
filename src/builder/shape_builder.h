#pragma once

#include "../circle.h"
#include "../triangle.h"
#include "../rectangle.h"
#include "../compound_shape.h"
#include <vector>
#include <stack>

class ShapeBuilder{
private:
    std::vector<Shape*> _result;
    std::stack<CompoundShape*> _compound_shape_stack;

public:

    void buildCircle(const Point *center, const Point *to_radius) {
        TwoDimensionalVector* vec = new TwoDimensionalVector(center, to_radius);
        Circle* circle = new Circle(vec);
        if (!this->_compound_shape_stack.empty())
            this->_compound_shape_stack.top()->addShape(circle);
        else     
            this->_result.push_back(circle);
    }

    void buildTriangle(const Point *common_point, const Point *v1_point, const Point *v2_point) {
        TwoDimensionalVector* vec1 = new TwoDimensionalVector(common_point, v1_point);
        TwoDimensionalVector* vec2 = new TwoDimensionalVector(common_point, v2_point);
        Triangle* triangle = new Triangle(vec1, vec2);
        if (!this->_compound_shape_stack.empty())
            this->_compound_shape_stack.top()->addShape(triangle);
        else
            this->_result.push_back(triangle);

    }

    void buildRectangle(const Point *common_point, const Point *v1_point, const Point *v2_point) {
        TwoDimensionalVector* vec1 = new TwoDimensionalVector(common_point, v1_point);
        TwoDimensionalVector* vec2 = new TwoDimensionalVector(common_point, v2_point);
        Rectangle* rectangle = new Rectangle(vec1, vec2);
        if (!this->_compound_shape_stack.empty())
            this->_compound_shape_stack.top()->addShape(rectangle);
        else
            this->_result.push_back(rectangle);
    }

    void buildCompoundShape() {
        CompoundShape* compound_shape = new CompoundShape();
        this->_compound_shape_stack.push(compound_shape);
    }

    void buildCompoundEnd() {
        CompoundShape* compound_shape = this->_compound_shape_stack.top();
        this->_compound_shape_stack.pop();
        if (!this->_compound_shape_stack.empty())
            this->_compound_shape_stack.top()->addShape(compound_shape);
        else
            this->_result.push_back(compound_shape);
    }

    std::vector<Shape*> getResult() { return this->_result; }
};
