#pragma once

#include <string>
#include <vector>

class Scanner
{
private:
    std::string _input;
    std::string::size_type _pos = 0;
    const std::vector<std::string> _tokenList = {"Circle", "Rectangle", "Triangle", "CompoundShape", "Vector", "(", ")", ","};
    void skipWhiteSpace() {
        while(_input[this->_pos] == ' ' || _input[this->_pos] == '\n')
            this->_pos++;
    }
    void numberAdd(std::string &result) {
        while (this->_input[this->_pos] >= '0' && this->_input[this->_pos] <= '9')
        {
            result += _input[this->_pos];
            this->_pos++;
        }
    }

public:
    Scanner(std::string input = "") : _input(input) {}

    std::string next() {
        if (!this->isDone()){
            std::string result = "";
            skipWhiteSpace();
            for (auto token : this->_tokenList){
                if (_input.compare(this->_pos, token.length(), token) == 0){
                    this->_pos += token.length();
                    result = token;
                    break;
                }
            }  
            return result; 
        }
        else
            throw "Is Done!";
    }

    double nextDouble() {
        if (!this->isDone()){
            std::string result = "";
            skipWhiteSpace();
            if (_input[this->_pos] == '-'){
                result += _input[this->_pos];
                this->_pos++;
            }
            numberAdd(result);
            if (_input[this->_pos] == '.'){
                result += _input[this->_pos];
                this->_pos++;
            }
            numberAdd(result);

            return std::stod(result);
        }
        else
            throw "Is Done!";
    }

    bool isDone() { return this->_pos == this->_input.length(); }
};
