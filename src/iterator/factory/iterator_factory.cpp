#include "iterator_factory.h"
#include "list_iterator_factory.h"
#include "bfs_iterator_factory.h"
#include "dfs_iterator_factory.h"

std::map<std::string, IteratorFactory*> IteratorFactory::_register_iterator_factory;

void IteratorFactory::_register(std::string name, IteratorFactory* factory){
    _register_iterator_factory[name] = factory;
}

IteratorFactory* IteratorFactory::getInstance(std::string name){
    return _register_iterator_factory[name];
}

