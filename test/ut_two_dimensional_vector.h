#include "../src/point.h"
#include "../src/two_dimensional_vector.h"

TEST(TwoDimensionalVectorTest, APointTest){
    Point* p1 = new Point(-8.42, 3.42);
    Point* p2 = new Point(-3.38, 4.3);
    TwoDimensionalVector v(p1, p2);
    Point a(-8.42, 3.42);
    ASSERT_EQ(a, *(v.a()));
    delete p1;
    delete p2;
}

TEST(TwoDimensionalVectorTest, BPointTest){
    Point* p1 = new Point(-8.42, 3.42);
    Point* p2 = new Point(-3.38, 4.3);
    TwoDimensionalVector v(p1, p2);
    Point b(-3.38, 4.3);
    ASSERT_EQ(b, *(v.b()));
    delete p1;
    delete p2;
}

TEST(TwoDimensionalVectorTest, LengthTest){
    Point* p1 = new Point(10, 5);
    Point* p2 = new Point(5, 8);
    TwoDimensionalVector v(p1, p2);
    ASSERT_NEAR(5.83, v.length(), 0.001);
    delete p1;
    delete p2;
}

TEST(TwoDimensionalVectorTest, DotTest){
    Point* p1 = new Point(3, 4);
    Point* p2 = new Point(5, 6);
    Point* p3 = new Point(1, 2);
    Point* p4 = new Point(10, 8);
    TwoDimensionalVector v1(p1, p2);
    TwoDimensionalVector v2(p3, p4);
    ASSERT_NEAR(30, v1.dot(&v2), 0.001);
    delete p1;
    delete p2;
    delete p3;
    delete p4;
}

TEST(TwoDimensionalVectorTest, CrossTest){
    Point* p1 = new Point(3, 4);
    Point* p2 = new Point(5, 6);
    Point* p3 = new Point(1, 2);
    Point* p4 = new Point(10, 8);
    TwoDimensionalVector v1(p1, p2);
    TwoDimensionalVector v2(p3, p4);
    ASSERT_NEAR(-6, v1.cross(&v2), 0.001);
    delete p1;
    delete p2;
    delete p3;
    delete p4;
}

TEST(TwoDimensionalVectorTest, InfoTest){
    Point* p1 = new Point(-8.42, 3.42);
    Point* p2 = new Point(-3, 4.3);
    TwoDimensionalVector v(p1, p2);
    ASSERT_EQ("Vector ((-8.42, 3.42), (-3.00, 4.30))", v.info());
    delete p1;
    delete p2;
}